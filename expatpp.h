/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2005-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
*/

#include <iosfwd>
#include <stdexcept>
#include <string>

namespace xml 
{

	class parser
	{
		class helper;
		friend class helper;
	public:
		parser() : ptr(0), parse_depth(0) {};
		virtual ~parser() {};

		void		parse( std::istream& in );
		unsigned	depth() const { return parse_depth; };
		unsigned long	current_byte_index() const;
		unsigned	current_line() const;
		unsigned	current_column() const;

	protected:
		virtual	void	element_start(char const *name, char const **attr );
		virtual void	element_end( char const* name );
		virtual void	character_data( char const* text, int len );
		virtual void	comment_data( char const* text );

		char const*	get_attribute( char const** attr, char const* name );

	private:
		void*		ptr;
		unsigned	parse_depth;
	};
	
	// To print out the expat version
	class expat_version 
	{
	};
	std::ostream& operator<<( std::ostream&, expat_version );
	
	// XML parsing exception
	class parse_error : public std::exception
	{
	public:
		parse_error( char const* message, parser const& p );
		parse_error( std::string const& message, parser const& p );
		parse_error( char const* message, unsigned line, unsigned column );
		parse_error( std::string const& message, unsigned line, unsigned column );
		~parse_error() throw();
		const char* what() const throw();
		
		unsigned	line() const { return line_; };
		unsigned	column() const { return column_; };
	private:
		std::string	message_;
		unsigned	line_;
		unsigned	column_;
	};	
}

