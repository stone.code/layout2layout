/*
** This code is distributed under a modified BSD license.
** Please refer to http://www.sourceforge.net/projects/adsm/
**
*************************************************************************
**
** Copyright (c) 2005-2006 Robert W. Johnstone
** All rights reserved.
**
*************************************************************************
**
** This file contains the bulk of the implementation for the conversion
** program layout2layout.  This tool is designed to convert layout files
** to other types of outputs, including images and masks.
**
*/

#pragma warning (disable:4786)

#include <layout.h>
#include <layout_export.h>
#include "expatpp.h"
#include <assert.h>
#include <exception>
#include <fstream>
#include <iostream>
#include <list>
#include <stdlib.h>
#include <string>

using namespace liblayout;

#define BAD_LAYER 0x80000000

static bool is_integer( char const* c_str );
extern void read_layers( char const* filename, cif::map_name_id&, cif::map_id_name&, svg::map_id_style&, psmask::map_id_darkfield& );
static void print_usage( std::ostream& );
static void print_version( std::ostream& );
static void process( char const* filename, std::string const& output_type, layer_id layer, manhattan_transform transform, cif::map_name_id&, cif::map_id_name&, svg::map_id_style&, psmask::map_id_darkfield&, double scale );

static double dxf_import_scale = 1.0;

/***********************************************************************/

int main( int argc, char* argv[] )
{
	std::string			output_type;
	cif::map_name_id		name_to_id;
	cif::map_id_name		id_to_name;
	svg::map_id_style		id_to_css;
	psmask::map_id_darkfield	id_to_darkfield;
	layer_id			layer = BAD_LAYER;
	double				scale = 0.0;
	manhattan_transform		transform = R0;

	// check that we have at least on parameter, this is in
	// addition to the program name
	if ( argc<2 ) 
	{
		print_usage( std::cerr );
		return EXIT_FAILURE;
	}

	// if we have exactly two arguments, we might be requesting
	// either help or verison information
	if ( argc==2 && argv[1][0]=='-' ) 
	{
		// there is one command-line argument
		// that argument is a command switch

		// first, ignore dash if there are two
		if ( argv[1][1]=='-' ) ++argv[1];
		assert( argv[1][0]=='-' );

		// is it a request for help?
		if ( tolower( argv[1][1] ) == 'h' )
		{
			print_usage( std::cout );
			return EXIT_SUCCESS;
		}

		// is is a request for version information?
		if ( tolower( argv[1][1] ) == 'v' )
		{
			print_version( std::cout );
			return EXIT_SUCCESS;
		}
	}
		
	// skip program name
	++argv;
	// get command line options
	while ( *argv )
	{
		// to simplify this loop
		// store the string currently being processed
		char const* str = *argv;

		if ( str[0] == '-' )
		{
			switch ( tolower(str[1]) )
			{
			case 'c':
				++argv;
				try {
					read_layers( *argv, name_to_id, id_to_name, id_to_css, id_to_darkfield );
				}
				catch ( std::exception& e ) {
					std::cerr << "ERROR: Could not read configuration file." << std::endl;
					std::cerr << "ERROR: " << e.what() << std::endl;
					return EXIT_FAILURE;
				}
				break;

			case 'd':
				++argv;
				dxf_import_scale = atof( *argv );
				if ( dxf_import_scale < 0.0 || !isdigit(**argv) ) {
					std::cerr << "ERROR: The DXF input scale must be a non-negative number." << std::endl;
					return EXIT_FAILURE;
				}
				break;

			case 'f':
				++argv;
				if ( is_integer( *argv ) && atol(*argv)>0 ) {
					psmask::font_size = atol( *argv );
				}
				else {
					std::cerr << "ERROR:  The font size must be a positive integer value." << std::endl;
					return EXIT_FAILURE;
				}
				break;

			case 'l':
				++argv;
				if ( is_integer( *argv ) ) {
					layer = atol( *argv );
				}
				else if ( name_to_id.find(*argv) != name_to_id.end() ) {
					layer = name_to_id[*argv];
				}
				else {
					layer = BAD_LAYER;
					std::cerr << "NOTE:  Layer " << *argv << " not recognized." << std::endl;
				}
				break;

			case 'm':
				++argv;
				// can't redo the layers
				if ( output_type.empty() )
					output_type = *argv;
				else {
					std::cerr << "WARN:  The output type has already been set to " << output_type << ".  Ignoring redefinition." << std::endl;
					return EXIT_FAILURE;
				}
				break;

			case 's':
				++argv;
				scale = atof( *argv );
				if ( scale < 0.0 || !isdigit(**argv) ) {
					std::cerr << "ERROR: The scale must be a non-negative number." << std::endl;
					return EXIT_FAILURE;
				}
				break;
				
			case 't':
				++argv;
				if ( is_manhattan_transform(*argv) ) {
					transform = get_manhattan_transform(*argv);
				}
				else {
					std::cerr << "ERROR:  The transform '" << *argv << "' is not a recognized manhattan transform." << std::endl;
					return EXIT_FAILURE;
				}
				break;
				
			default:
				std::cerr << "ERROR:  The command-line switch '" << *argv << "' is not recognized.\n\n";
				print_usage( std::cerr );
				return EXIT_FAILURE;
			}
		}
		else
		{
			// make sure that an output type was requested
			if ( output_type.empty() )
			{
				std::cout << "ERROR:  No output type was specified." << std::endl;
				return EXIT_FAILURE;
			}

			// we are going to process a file
			process( *argv, output_type, layer, transform, name_to_id, id_to_name, id_to_css, id_to_darkfield, scale );
			break;
		}

		// move to next argument
		++argv;
	}

	return EXIT_SUCCESS;
}

static void on_comment( void* _unused, char const* comment )
{
	std::cout << comment << '\n';	
}

static void on_layer( void* _unused, char const* name, layer_id id )
{
	std::cout << "Unrecognized layer '" << name << "' added with an id of " << id << ".\n";
}

static void process( char const* filename, std::string const& output_type, layer_id layer, manhattan_transform transform, cif::map_name_id& name_to_id, cif::map_id_name& id_to_name, svg::map_id_style& id_to_css, psmask::map_id_darkfield& id_to_df, double scale )
{
	// perform conversion
	try 
	{
		std::list<layout>	layouts;
		layout*			top_layout = 0;

		// import the layout
		if ( gdsii::is_file_gdsii( filename ) )
		{
			double gdsii_scale = 1.0;
			top_layout = gdsii::import( layouts, &gdsii_scale, on_comment, 0, filename );
			std::cout << "Input GDSII file reports scale is " << gdsii_scale << "." << std::endl;
		}
		else if ( cif::is_file_cif( filename ) )
		{
			top_layout = cif::import( layouts, name_to_id, 
				on_comment, 0, 
				on_layer, 0,
				filename );
		}
		else if ( dxf::is_file_dxf( filename ) )
		{
			top_layout = dxf::import( layouts, name_to_id, dxf_import_scale,
				on_comment, 0,
				on_layer, 0,
				filename );
			if ( dxf_import_scale != 1 )
				scale /= dxf_import_scale;
		}
		else
		{
			std::cerr << "ERROR:  Could not process file '" << filename << "'." << std::endl;
			std::cerr << "ERROR:  The input file must represent a layout in either CIF, GDSII, or DXF format." << std::endl;
			return;
		}
		assert( top_layout );
		
		// make sure the layout has a name, which is used
		// by some drivers for metadata
		if ( !top_layout->get_name() || !top_layout->get_name()[0] ) {
			top_layout->set_name( filename );
		}
		assert( top_layout->get_name() );
		assert( top_layout->get_name()[0] );
		std::cout << "Processing layout titled \"" << top_layout->get_name() << "\"." << std::endl;

		// Reflect any name names that may have been inserted into the reverse mapping.
		cif::update_id_names( id_to_name, name_to_id );
		
		// some diagnostics
		if ( top_layout->has_bounds() ) {
			rect bounds = top_layout->bounds();
			std::cout << "Bounds: " << bounds.x1 << ", " << bounds.y1 << " to " << bounds.x2 << ", " << bounds.y2 << std::endl;
			if ( scale > 0 )
				std::cout << "Size: " << (bounds.x2-bounds.x1)*scale << " by " << (bounds.y2-bounds.y1)*scale << " metre" << std::endl;
			else
				std::cout << "Size:  not available (driver will attempt to auto-scale)." << std::endl;
		}
		else {
			std::cout << "Bounds unavailable.  Is the layout empty?" << std::endl;
		}
		
		// apply transform if required
		if ( transform != R0 ) 
		{
			layouts.push_back( layout() );
			layouts.back().set_name( top_layout->get_name() );
			layouts.back().insert( top_layout, transform );
			top_layout = & layouts.back();
			assert( top_layout->get_name() );
			assert( top_layout->get_name()[0] );
			assert( top_layout->shapes_size() == 0 );
			assert( top_layout->composites_size() == 1 );
		}

		// create the output
		if ( output_type == "application/ansys" )
		{
			if ( layer == BAD_LAYER )
				ansys::write( *top_layout, (std::string(filename) + ".anf").c_str() );
			else
				ansys::write( *top_layout, layer, (std::string(filename) + ".anf").c_str() );
		}
		else if ( output_type == "application/asymptote" )
		{
			if ( layer == BAD_LAYER )
				asy::write( *top_layout, id_to_css, scale, (std::string(filename) + ".asy").c_str() );
			else
				asy::write( *top_layout, layer, id_to_css, scale, (std::string(filename) + ".asy").c_str() );
		}
		else if ( output_type == "application/cif" )
		{
			cif::write( *top_layout, id_to_name, true, (std::string(filename) + ".cif").c_str() );
		}
		else if ( output_type == "application/dxf" )
		{
			if ( scale <= 0 ) {
				std::cout << "WARNING:  Scale factor not set for DXF output.  Assuming a value of 1e-9\n";
				scale = 1e-9;
			}
			assert( scale > 0 );
			if ( layer == BAD_LAYER )
				dxf::write( *top_layout, id_to_name, scale, (std::string(filename) + ".dxf").c_str() );
			else
				dxf::write( *top_layout, layer, id_to_name, scale, (std::string(filename) + ".dxf").c_str() );
		}
		else if ( output_type == "application/gdsii" )
		{
			if ( scale <= 0 ) {
				std::cout << "WARNING:  Scale factor not set for GDSII output.  Assuming a value of 1e-9\n";
				scale = 1e-9;
			}
			assert( scale > 0 );
			gdsii::write( *top_layout, scale, (std::string(filename) + ".gdsii").c_str() );
		}
		else if ( output_type == "application/gerber" )
		{
			if ( layer == BAD_LAYER )
				gerber::write( *top_layout, scale, (std::string(filename) + ".gbr").c_str() );
			else
				gerber::write( *top_layout, layer, scale, (std::string(filename) + ".gbr").c_str() );
		}
		else if ( output_type == "application/iges" )
		{
			if ( layer == BAD_LAYER )
				iges::write( *top_layout, (std::string(filename) + ".iges").c_str() );
			else
				iges::write( *top_layout, layer, (std::string(filename) + ".iges").c_str() );
		}
		else if ( output_type == "application/postscript" )
		{
			if ( layer == BAD_LAYER )
				ps::write( *top_layout, scale, id_to_css, (std::string(filename) + ".ps").c_str() );
			else
				ps::write( *top_layout, layer, scale, id_to_css, (std::string(filename) + ".ps").c_str() );
		}
		else if ( output_type == "application/postscript+mask" )
		{
			std::string outfilename( std::string(filename) + ".ps" );
			if ( layer == BAD_LAYER )
				psmask::write( *top_layout, id_to_df, id_to_name, scale, outfilename.c_str() );
			else
				psmask::write( *top_layout, layer, id_to_df, id_to_name, scale, outfilename.c_str() );
		}
		else if ( output_type == "image/svg" )
		{
			if ( layer == BAD_LAYER )
				svg::write( *top_layout, id_to_css, scale, (std::string(filename) + ".svg").c_str() );
			else
				svg::write( *top_layout, layer, id_to_css, scale, (std::string(filename) + ".svg").c_str() );
		}
		else if ( output_type == "image/wmf" )
		{
			if ( layer == BAD_LAYER )
				wmf::write( *top_layout, id_to_css, scale, (std::string(filename) + ".wmf").c_str() );
			else
				wmf::write( *top_layout, layer, id_to_css, scale, (std::string(filename) + ".wmf").c_str() );
		}
		else if ( output_type == "text/plain" )
		{
			if ( layer == BAD_LAYER )
				text::write( *top_layout, id_to_name, (std::string(filename) + ".txt").c_str() );
			else
				text::write( *top_layout, layer, id_to_name, (std::string(filename) + ".txt").c_str() );
		}
		else
		{
			std::cerr << "Could not process file '" << filename << "'." << std::endl;
			std::cerr << "Output type mime specification unrecognized." << std::endl;
		}
	}
	catch ( std::exception& e )
	{
		std::cerr << "ERROR: Could not process file '" << filename << "'." << std::endl;
		std::cerr << "ERROR: " << e.what() << std::endl;
	}
	catch ( ... )
	{
		std::cerr << "ERROR: Could not process file '" << filename << "'." << std::endl;
		std::cerr << "ERROR: " << "Unknown error." << std::endl;
	}
}

static bool is_integer( char const* c_str )
{
	while ( *c_str )
	{
		if ( ! *c_str ) return false;
		if ( ! isdigit(*c_str) ) return false;
		++c_str;
	};

	return true;
}

static void print_usage( std::ostream& out )
{
	out << "Usage for layout2layout:" << std::endl;
	out << "\tlayout2layout -m mime_type [option]... file [[option]... file]...\n\n";
	out << "The mime_type is any of the following:\n";
	out << "\tapplication/ansys\n";
	out << "\tapplication/asy\n";
	out << "\tapplication/cif\n";
	out << "\tapplication/dxf\n";
	out << "\tapplication/gdsii\n";
	out << "\tapplication/gerber\n";
	out << "\tapplication/iges\n";
	out << "\tapplication/postscript\n";
	out << "\tapplication/postscript+mask\n";
	out << "\timage/svg\n";
	out << "\timage/wmf\n";
	out << "\ttext/plain\n\n";
	out << "Additional possible options include:\n";
	out << "\t-c file:   A configure file to describe layers\n";
	out << "\t-f size:   Font size (in points) used to label PDF masks\n";
	out << "\t-l layer:  Convert a single layer, described by id or name\n";
	out << "\t-s scale:  Conversion factor between DB units used by layout and meters.\n\n";
	out << "refer to http://bitbucket.org/rj/layout2layout/wiki/usage for more information.\n" << std::endl;
}

static void print_version( std::ostream& out )
{
	out << PACKAGE_NAME ": " PACKAGE_VERSION << "\n";
	out << "\tusing " << liblayout_version_zstring() << '\n';
	out << "\tusing " << xml::expat_version() << std::endl;
}
